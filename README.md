# WishListWebApi

https://mywl-api.herokuapp.com/home

Consulte a documentação para mais detalhes!!

[Clique aqui para acessar a documentação.](https://gitlab.com/Simplicio00/wishlistwebapi/-/blob/master/Documentacao%20API.pdf)

# Informações

Tecnologia: ASP NET CORE (.NET 5)

Período de desenvolvimento:  -- 05/2021

Tipo: API (Interface provedora de serviços)


# Compilando localmente 

*Requisito : **.NET 5** 

Navegue até _/wlapi/_

Dentro da pasta, execute o comando

`dotnet run`

Faça requisições pelo seu localhost
