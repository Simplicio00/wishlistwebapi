﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using wlapi.Model;
using wlapi.ViewModel;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;

namespace wlapi.Data
{
	public class DataManager
	{

		/// <summary>
		/// Create a new user with the given credentials
		/// </summary>
		/// <param name="newUser">user to be inserted in the database</param>
		public static void InsertNewUser(User newUser)
		{
			string users = Directory.GetCurrentDirectory() + "/user/User.txt";

			if (!File.Exists(users))
			{
				GetEmailList();
			}

			try
			{
				string data =  $"{File.ReadLines(users).ToList().Count() + 1 }>>{newUser.Email}>>{newUser.Name}>>{newUser.Password}";

				File.AppendAllLines(users, new[] { data } );

			}
			catch (Exception)
			{
				throw;
			}

		}


		/// <summary>
		/// Make a logon action to generate personal token
		/// </summary>
		/// <param name="credentials">Credentials to login</param>
		/// <returns>Returns the autentication token</returns>
		public static string Login(LoginViewModel credentials)
		{
			string users = Directory.GetCurrentDirectory() + "/user/User.txt";

			if (!File.Exists(users))
			{
				GetEmailList();
			}

			List<User> dblist = new List<User>(); 

			try
			{
				foreach (var item in File.ReadAllLines(users))
				{
					var splitdata = item.Split(new[] { ">>" }, StringSplitOptions.None);

					dblist.Add(new User()
					{
						Id = int.Parse(splitdata[0]),
						Email = splitdata[1],
						Name = splitdata[2],
						Password = splitdata[3]
					});
				}

				var usr = dblist.FirstOrDefault(a => a.Email == credentials.Email && a.Password == credentials.Password);
				
				if (usr != null)
				{
					var claims = new[]
					{
						new Claim("id", usr.Id.ToString()),
						new Claim("email", usr.Email),
						new Claim("name", usr.Name),
						new Claim(ClaimTypes.Role, "user")
					};

					var key = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes("auth-wish-tokentest"));
					var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
					var token = new JwtSecurityToken(
					   issuer: "wlapi",
					   audience: "wlapi",
					   claims: claims,
					   expires: DateTime.Now.AddMinutes(30),
					   signingCredentials: creds
					);


					return new JwtSecurityTokenHandler().WriteToken(token);

				}


				return null;

			}
			catch (Exception)
			{
				throw;
			}
		}


		/// <summary>
		/// Return a list of emails that already exists in the database
		/// </summary>
		/// <returns>Return an email's list</returns>
		public static List<string> GetEmailList()
		{
			string users = Directory.GetCurrentDirectory() + "/user/User.txt";

			if (!File.Exists(users))
			{
				Directory.CreateDirectory(Directory.GetCurrentDirectory() + "/user");

				File.CreateText(users).Close();

				return new List<string>();
			}
			else
			{
				List<string> emaillist = new List<string>();

				foreach (var item in File.ReadLines(users).ToList())
				{
					var splitdata = item.Split(new[] { ">>" }, StringSplitOptions.None);

					emaillist.Add(splitdata[1]);
				}

				return emaillist;
			}

			
		}


		/// <summary>
		/// Get products list
		/// </summary>
		/// <returns>Returns a list of active products</returns>
		public static List<Product> GetProducts()
		{
			string product =  Directory.GetCurrentDirectory() + "/product/Product.txt";


			if(!File.Exists(product)){
				
				string NewDirectory = Directory.GetCurrentDirectory() + "/product/";

				Directory.CreateDirectory(NewDirectory);

				File.CreateText(Path.Combine(NewDirectory, "Product.txt")).Close();

				return null;
			}
			else
			{
				List<Product> products = new List<Product>();

				foreach (var nome in File.ReadLines(product).ToList())
				{
					var splitFileLine = nome.Split(new[] { ">>" }, StringSplitOptions.None);

					products.Add(new Product()
					{
						Id = int.Parse(splitFileLine[0]),
						Name = splitFileLine[1],
						Description = splitFileLine[2],
						Image = splitFileLine[3]
					}); 
				}

				return products;
				
			}

		}



		/// <summary>
		/// Insert a new product into the database
		/// </summary>
		/// <param name="product">New product to be inserted</param>
		public static void InsertNewProduct(Product product)
		{
			string dataDir = Directory.GetCurrentDirectory() + "/product/Product.txt";

			if (!File.Exists(dataDir))
			{
				GetProducts();
			}

			if (string.IsNullOrWhiteSpace(product.Image))
			{
				product.Image = "https://images.immediate.co.uk/production/volatile/sites/30/2020/08/orange-juice-7627168.jpg?quality=90&resize=620%2C310";
			}

			string data = $"{File.ReadLines(dataDir).ToList().Count + 1}>>{product.Name}>>{product.Description}>>{product.Image}";

			File.AppendAllLines(dataDir, new[] { data });
		}




		/// <summary>
		/// Retrieve information about products in the user wish's list 
		/// </summary>
		/// <param name="userId">User identification</param>
		/// <returns>Returns the user wish list</returns>
		public static List<WishUserList> GetWishList(int userId)
		{
			string wlDir = Directory.GetCurrentDirectory() + "/wishUserList/WishUserList.txt";

			if (!File.Exists(wlDir))
			{
				Directory.CreateDirectory(Directory.GetCurrentDirectory() + "/wishUserList");

				File.CreateText(wlDir).Close();

				return null;
			}
			else
			{
				List<WishUserList> ListWishes = new List<WishUserList>();

				foreach (var item in File.ReadLines(wlDir).ToList())
				{
					var splitFileLine = item.Split(new[] { ">>" }, StringSplitOptions.None);

					ListWishes.Add(new WishUserList() 
					{ 
						Id = int.Parse(splitFileLine[0]),
						IdProduct = int.Parse(splitFileLine[1]),
						IdUser = int.Parse(splitFileLine[2]),
						Status = splitFileLine[3]
					});
				}

				var mywishlist = ListWishes.Where(a => a.IdUser == userId).ToList();
				List<WishUserList> definitiveList = new List<WishUserList>();
				List<Product> allProducts = new List<Product>();


				foreach (var item in GetProducts())
				{
					allProducts.Add(item);
				}

				foreach (var item in mywishlist)
				{
					//search product
					Product product = allProducts.FirstOrDefault(pd => pd.Id == item.IdProduct);

					definitiveList.Add(new WishUserList()
					{
						Id = item.Id,
						IdProduct = item.IdProduct,
						IdUser = item.IdUser,
						Status = item.Status,
						IdProductNavigation = product
					});
				}

				return definitiveList;
				
			}

			

		}


		/// <summary>
		/// Insert a specific product in the wish list
		/// </summary>
		/// <param name="wl">action of insertion</param>
		public static bool InsertProductToWishList(WishUserList wl)
		{
			string wlDir = Directory.GetCurrentDirectory() + "/wishUserList/WishUserList.txt";

			if (!File.Exists(wlDir))
			{
				Directory.CreateDirectory(Directory.GetCurrentDirectory() + "/wishUserList");

				File.CreateText(wlDir).Close();

				return false;
			}
			else
			{
				if (string.IsNullOrEmpty(wl.Status))
				{
					wl.Status = "Padrão";
				}

				var products = GetProducts();

				if (products.FirstOrDefault(a => a.Id == wl.IdProduct) != null)
				{
					string data = $"{File.ReadLines(wlDir).ToList().Count + 1}>>{wl.IdProduct}>>{wl.IdUser}>>{wl.Status}";

					File.AppendAllLines(wlDir, new[] { data });

					return true;
				}

				return false;
			}

			


		}



	}
}
