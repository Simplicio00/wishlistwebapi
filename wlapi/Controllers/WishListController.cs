﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using wlapi.Data;
using wlapi.Model;
using wlapi.ViewModel;

namespace wlapi.Controllers
{
	[Route("[controller]")]
	[ApiController]
	public class WishListController : ControllerBase
	{


		[Authorize(Roles = "user")]
		[HttpGet]
		public IActionResult GetWishList() => Ok(DataManager.GetWishList(Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(usr => usr.Type == "id").Value)));

		

		[Authorize(Roles = "user")]
		[HttpPost("add")]
		public IActionResult AddProductToWishList([FromBody] AddToWish wishProcess)
		{
			var result = DataManager.InsertProductToWishList(new WishUserList()
			{
				IdProduct = wishProcess.idProduct,
				IdUser = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(usr => usr.Type == "id").Value),
				Status = wishProcess.Status
			});

			if (result.Equals(true))
			{
				return StatusCode(200, new { message = "The product went sucessfully to your wish's list" });
			}

			return StatusCode(400, new { message = "An error ocurred trying to add the product to your wish	s list, try again later.." });
		}





	}
}
