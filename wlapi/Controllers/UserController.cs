﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using wlapi.Data;
using wlapi.Model;
using wlapi.ViewModel;


namespace wlapi.Controllers
{
	[Route("[controller]")]
	[ApiController]
	public class UserController : ControllerBase
	{

		[AllowAnonymous]
		[HttpPost("new")]
		public IActionResult CreateAccount([FromBody] User user)
		{
			var validation = Utils.UserValidations.IntegratedUserValidation(user);

			if (!string.IsNullOrWhiteSpace(validation)) return StatusCode(401, new { message = validation });

			if (Utils.UserValidations.MailValidation(DataManager.GetEmailList(), user.Email).Equals(false)) return StatusCode(401, new { message = "The inserted E-Mail already exists, try another one..." });
			

			DataManager.InsertNewUser(user);


			return StatusCode(201, new { message = "Your account has been created sucessfully!" });

		}


		[AllowAnonymous]
		[HttpPost("logon")]
		public IActionResult Login([FromBody] LoginViewModel login)
		{
			string tokenJwt = DataManager.Login(login);

			if (!string.IsNullOrEmpty(tokenJwt))
			{
				return StatusCode(200, new { message = "Welcome", tokenJwt });
			}

			return StatusCode(401, new { message = "The system did not find the user, try again.." });
		}


		



	}
}
