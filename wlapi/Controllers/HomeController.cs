﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using wlapi.Data;
using wlapi.Model;

namespace wlapi.Controllers
{
	[Route("[controller]")]
	[ApiController]
	public class HomeController : ControllerBase
	{

		[AllowAnonymous]
		[HttpGet]
		public IActionResult GetListProducts() => Ok(DataManager.GetProducts());
		
		[AllowAnonymous]
		[HttpPost("new")]
		public IActionResult CreateNewProduct([FromBody] Product pd)
		{
			DataManager.InsertNewProduct(pd);

			return StatusCode(201, new { message = "The product is sucessfully inserted in the system!" });
		}


		[AllowAnonymous]
		[HttpGet("test")]
		public IActionResult TestResponse() => StatusCode(200, new { message = "Route testing.... Done!"});

	}
}
