﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using wlapi.Model;

namespace wlapi.Utils
{
	public class UserValidations
	{
		public static bool MailValidation(IEnumerable<string> list, string email)
			=> list.Where(a => a == email).Any() ? false : true;

		public static bool IsEmailTrue(string email)
			=> System.Text.RegularExpressions.Regex.IsMatch(email, "@") ? true : false;


		public static string IntegratedUserValidation(User user)
		{
			StringBuilder myErrors = new StringBuilder();

			if (user.Email.Length < 5)
			{
				myErrors.Append ($" \n '{user.Email}' does not have a valid format or it's null!");
			}

			if (user.Name.Length < 5)
			{
				myErrors.Append($" \n '{user.Name}' does not have a valid format or it's null!");
			}

			if (user.Password.Length < 5)
			{
				myErrors.Append($" \n your password does not have a valid format or it's null!");
			}


			if (IsEmailTrue(user.Email).Equals(false))
			{
				 myErrors.Append($" \n Wrong format of E-Mail address, put the '@' in the address format ");
			}

			return myErrors.ToString();

		}

	}
}
