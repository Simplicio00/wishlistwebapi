using System.ComponentModel.DataAnnotations;

namespace wlapi.ViewModel
{
    public class AddToWish
    {
        private const string uppthelimit = "The quantity of characters surpassed the maximum of permited lenght";
		private const string belowthelimit = "The quantity of characters inserted is below the limit permited...";

        
        [Required]
        public int idProduct {get;set;}

        [Required]
        [MinLength(5, ErrorMessage = belowthelimit)]
		[MaxLength(40, ErrorMessage = uppthelimit)]
        public string Status {get;set;}
    }
}