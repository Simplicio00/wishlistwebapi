﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using wlapi.Model;

namespace wlapi.ViewModel
{
	public class UserValidations
	{
		public static bool MailValidation(IEnumerable<string> list, string email)
			=> list.Where(a => a == email).Any() ? false : true;

		public static bool IsEmailTrue(string email)
			=> Regex.IsMatch(email, "@") ? true : false;


		public static bool PropertyIsValid(string valid) => valid.Trim().Length >= 6 ? true : false;
		



		public static string IntegratedUserValidation(User user)
		{
			StringBuilder myErrors = new StringBuilder();

			if (PropertyIsValid(user.Email.ToLower()).Equals(false))
			{
				myErrors.Append ($" \n  '{user.Email}' does not have a valid format or it's null!");
			}

			if (PropertyIsValid(user.Name.ToLower()).Equals(false))
			{
				myErrors.Append($" \n  '{user.Name}' does not have a valid format or it's less than permitted!");
			}

			if (PropertyIsValid(user.Password.ToLower()).Equals(false))
			{
				myErrors.Append($" \n  'Your inserted password' does not have a valid format or it's less than permitted!");
			}

			if (IsEmailTrue(user.Email).Equals(false))
			{
				 myErrors.Append($" \n  Wrong format of E-Mail address, put the '@' in the address format");
			}

			return myErrors.ToString();

		}

	}
}
