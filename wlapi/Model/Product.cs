﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace wlapi.Model
{
	public class Product
	{
		private const string uppthelimit = "The quantity of characters surpassed the maximum of permited lenght";
		private const string belowthelimit = "The quantity of characters inserted is below the limit permited...";

		public int Id { get; set; }

		[Required]
		[MinLength(5, ErrorMessage = belowthelimit)]
		[MaxLength(40, ErrorMessage = uppthelimit)]
		public string Name { get; set; }

		[Required]
		[MinLength(5, ErrorMessage = belowthelimit)]
		[MaxLength(300, ErrorMessage = uppthelimit)]
		public string Description { get; set; }


		public string Image { get; set; }

	}
}
