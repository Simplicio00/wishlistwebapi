﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace wlapi.Model
{
	public class WishUserList
	{
		public int Id { get; set; }
		public int IdUser { get; set; }
		public int IdProduct { get; set; }
		public string Status { get; set; }


		public virtual Product IdProductNavigation { get; set; }
	}
}
