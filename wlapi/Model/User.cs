﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace wlapi.Model
{
	public class User
	{
		private const string uppthelimit = "The quantity of characters surpassed the maximum of permited lenght";
		private const string belowthelimit = "The quantity of characters inserted is below the limit permited...";

		public int Id { get; set; }
		
		[Required]
		[MinLength(5, ErrorMessage = belowthelimit)]
		[MaxLength(40, ErrorMessage = uppthelimit)]
		public string Name { get; set; }

		[Required]
		[MinLength(5, ErrorMessage = belowthelimit)]
		[MaxLength(40, ErrorMessage = uppthelimit)]
		public string Email { get; set; }

		[Required]
		[MinLength(5, ErrorMessage = belowthelimit)]
		[MaxLength(10, ErrorMessage = uppthelimit)]
		public string Password { get; set; }


	}
}
